package controller;

import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import javax.swing.JOptionPane;

public class ArquivosController implements IArquivosController {

	@Override
	public void leDir(String path) throws IOException {
		File dir = new File(path);
		if(dir.exists() && dir.isDirectory()) {
			File[] files = dir.listFiles();
			for (File f: files) {
				if(f.isFile()) {
					System.out.println("     \t" + f.getName());
				}
				else {
					System.out.println("<DIR>\t" + f.getName());
				}
			}
		}
		else {
			throw new IOException("Diretório inválido");
		}
	}

	@Override
	public void criaArquivo(String path, String name) throws IOException {
		File dir = new File(path);
		File arq = new File(path, name + ".txt");
		if(dir.exists() && dir.isDirectory()) {
			boolean existe = false;
			if(arq.exists()) {
				existe = true;
			}
			String conteudo = geraTxt();
			FileWriter filewriter = new FileWriter(arq, existe);
			PrintWriter print = new PrintWriter(filewriter);
			print.write(conteudo);
			print.flush();
			print.close();
			filewriter.close();
			
		}
		else {
			throw new IOException("Diretório Inválido");
		}
	}

	private String geraTxt() {
		StringBuffer buffer = new StringBuffer();
		String linha = "";
		while(!linha.equalsIgnoreCase("Fim")) {
			linha = JOptionPane.showInputDialog(null, "Digite uma Frase", "Entrada de texto", JOptionPane.INFORMATION_MESSAGE);
			if(!linha.equalsIgnoreCase("Fim")) {
				buffer.append(linha + "\r\n");
			}
		}
		return buffer.toString();
	}

	@Override
	public void leArquivo(String path, String name) throws IOException {
		File arq = new File(path, name);
		if(arq.exists() && arq.isFile()) {
			FileInputStream fluxo = new FileInputStream(arq);
			InputStreamReader leitor = new InputStreamReader(fluxo);
			BufferedReader buffer = new BufferedReader(leitor);
			String linha = buffer.readLine();
			while(linha != null) { // procurando EOF (End of File)
				System.out.println(linha);
				linha = buffer.readLine();
			}
			buffer.close();
			leitor.close();
			fluxo.close();
		}
		else {
			throw new IOException("Arquivo Inválido");
		}
	}
	public void transferenciaConteudo(String path, String name, String destiny) throws IOException {
		File dir = new File(path);
		File arq = new File(path, name);
		File arq2 = new File(path, destiny);
		boolean existe = false;
		int x = 0;
		if(dir.exists() && dir.isDirectory()) {
			if(arq.exists() && arq.isFile()) {
				if(arq2.exists() && arq2.isFile()) {
					existe = true;
				}
				BufferedReader buffer = new BufferedReader(new FileReader(arq));
				String linha = buffer.readLine();
				FileWriter filewriter = new FileWriter(arq2,existe);
				while(linha != null) {
					linha.replaceAll(" ", ",");
					String conteudo[] = linha.split(",");
					while(x < conteudo.length) {
						filewriter.append(conteudo[x]);
						filewriter.append(",");
						x++;
					}
					filewriter.append("\n");
					linha = buffer.readLine();
					x = 0;
				}
				filewriter.flush();
				filewriter.close();
				buffer.close();
			}
			else {
				throw new IOException("Arquivo Inválido");
			}
		}
		else {
			throw new IOException("Diretório Inválido");
		}
	}

	@Override
	public void abreArquivo(String path, String name) throws IOException {
		File arq = new File(path, name);
		if(arq.exists() && arq.isFile()) {
			Desktop desktop = Desktop.getDesktop();
			desktop.open(arq);
		}
		else {
			throw new IOException("Arquivo Inválido");
		}
	}

}