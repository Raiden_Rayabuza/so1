package controller;

import java.io.IOException;

public interface IArquivosController {
	public void leDir(String path) throws IOException;
	public void criaArquivo(String path, String name) throws IOException;
	public void leArquivo(String path, String name) throws IOException;
	public void abreArquivo(String path, String name) throws IOException;
	public void transferenciaConteudo(String path, String name, String destiny)throws IOException;
}
