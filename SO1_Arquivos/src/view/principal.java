package view;
import java.io.IOException;

import controller.ArquivosController;
import controller.IArquivosController;
public class principal {

	public static void main(String[] args) {
		IArquivosController arqCont = new ArquivosController();
		String path = "C:\\Users\\USER\\Downloads";
		String name = "relatorio.txt";
		String destiny = "relatorio.csv";
		try {
			//arqCont.leDir(dir);
			//arqCont.criaArquivo(path, name);
			//arqCont.leArquivo(path, name);
			arqCont.abreArquivo(path, name);
			arqCont.transferenciaConteudo(path, name, destiny);
			arqCont.abreArquivo(path, destiny);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
