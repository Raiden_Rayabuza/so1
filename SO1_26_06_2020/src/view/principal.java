package view;
import java.io.IOException;

import javax.swing.JOptionPane;

import controller.ArquivosController;
import controller.IArquivosController;
public class principal {

	public static void main(String[] args) {
		IArquivosController arqCont = new ArquivosController();
		String arquivo = "Cadastro.csv";
		int codigo = 0;
		String nome = "";
		String email = "";
		int opt = 0;
		do {
			opt = Integer.parseInt(JOptionPane.showInputDialog("Selecione a fun��o que deseja realizar:\n\n1-Verificar e/ou a existencia do diret�rio e arquivo\n2-Inserir um registro\n3-Imprimir um registro\n4-Verificar a existencia de um registro\n\n0-Para encerrar o programa"));
			switch(opt) {
				case 1:
					try {
						arqCont.verificaDirTemp();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					break;
				case 2:
					codigo = Integer.parseInt(JOptionPane.showInputDialog("Inserir o codigo do registro"));
					nome = String.valueOf(JOptionPane.showInputDialog("Inserir o nome do registro"));
					email = String.valueOf(JOptionPane.showInputDialog("Inserir o email do registro"));
					try {
						arqCont.insereCadastro(arquivo, codigo, nome, email);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					break;
				case 3:
					codigo = Integer.parseInt(JOptionPane.showInputDialog("Inserir o codigo do registro que deseja imprimir"));
					try {
						arqCont.imprimeCadastro(arquivo, codigo);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					break;
				case 4:
					codigo = Integer.parseInt(JOptionPane.showInputDialog("Inserir o codigo do registro que deseja verificar a existencia"));
					try {
						arqCont.verificaRegistro(arquivo, codigo);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					break;
			}
		}while(opt != 0);
	}
}
