package controller;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

public class ArquivosController implements IArquivosController {

	@Override
	public void verificaDirTemp() throws IOException {
		File dir = new File("C:\\TEMP");
		if(dir.exists() && dir.isDirectory()) {
			criaArquivos();
			System.out.println("O diret�rio TEMP j� existe");
		}
		else {
			dir.mkdir();
			criaArquivos();
			System.out.println("O diret�rio TEMP foi criado");
		}
	}
	
	private void criaArquivos() throws IOException {
		File file = new File("C:\\TEMP","Cadastro.csv");
		if(file.exists() && file.isFile()) {
			System.out.println("O arquivo j� foi criado");
		}
		else {
			file.createNewFile();
			FileWriter escreve = new FileWriter(file,false);
			escreve.write("(Codigo | Nome | Email)");
			escreve.flush();
			escreve.close();
			System.out.println("O arquivo 'Cadastro.csv' foi criado");
		}
	}
	@Override
	public boolean verificaRegistro(String arquivo, int codigo) throws IOException {
		File dir = new File("C:\\TEMP");
		File file = new File("C:\\TEMP",arquivo);
		boolean existe = false;
		if(dir.exists() && dir.isDirectory()) {
			if(file.exists() && file.isFile()) {
				FileInputStream arq = new FileInputStream(file);
				InputStreamReader leitor = new InputStreamReader(arq);
				BufferedReader buffer = new BufferedReader(leitor);
				String registros = buffer.readLine();
				while(registros != null) {
					if(registros.contains(String.valueOf(codigo))) {
						existe = true;
						break;
					}
					else {
						existe = false;
					}
					registros = buffer.readLine();
				}
				if(existe == true) {
					System.out.println("O registro j� existente");
				}
				else {
					System.out.println("O registro n�o existe e pode ser inserido");
				}
				buffer.close();
				leitor.close();
				arq.close();
			}
			else {
				throw new IOException("O arquivo n�o existe");
			}
		}
		else {
			throw new IOException("O diret�rio TEMP n�o existe");
		}
	
		return existe;
	}

	@Override
	public void imprimeCadastro(String arquivo, int codigo) throws IOException {
		File dir = new File("C:\\TEMP");
		File file = new File("C:\\TEMP",arquivo);
		boolean existe = false;
		int x = 0;
		if(dir.exists() && dir.isDirectory()) {
			if(file.exists() && file.isFile()) {
				existe = verificaRegistro(arquivo, codigo);
				if(existe == true) {
					FileInputStream fluxo = new FileInputStream(file);
					InputStreamReader leitor = new InputStreamReader(fluxo);
					BufferedReader buffer = new BufferedReader(leitor);
					String registros = buffer.readLine();
					while(registros != null) {
						String reg[] = registros.split(" ");
						if(reg[0].contains(String.valueOf(codigo))) {
							System.out.println("Codigo: " + reg[0]);
							System.out.println("Nome: " + reg[1]);
							System.out.println("Email: " + reg[2]);
							break;
						}
						registros = buffer.readLine();
					}
					buffer.close();
					leitor.close();
					fluxo.close();
				}
				else {
					throw new IOException("O registro n�o existe");
				}
			}
			else {
				throw new IOException("O arquivo n�o existe");
			}
		}
		else {
			throw new IOException("O diret�rio 'TEMP' n�o existe");
		}
	}

	@Override
	public void insereCadastro(String arquivo, int codigo, String nome, String email) throws IOException {
		File dir = new File("C:\\TEMP");
		File file = new File("C:\\TEMP", arquivo);
		boolean existe = verificaRegistro(arquivo, codigo);
		if(dir.exists() && dir.isDirectory()) {
			if(file.exists() && file.isFile()) {
				if(existe == false) {
					FileWriter escreve = new FileWriter(file,true);
					escreve.append("\n" + codigo + " " + nome + " " + email);
					escreve.flush();
					escreve.close();
				}
				else {
					throw new IOException("O codigo adicionado j� existe no arquivo");
				}
			}
			else {
				throw new IOException("O arquivo n�o existe");
			}
		}
		else {
			throw new IOException("O diret�rio 'TEMP' n�o existe");
		}
		
	}

}