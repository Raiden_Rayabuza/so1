package Jokenpo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class Jokenpo_Main {

	public static void main(String[] args) {
		ArrayList<Integer> list = new ArrayList<Integer>();
		Random ale = new Random();
        for (int i = 0; i < 11; i++) {
            list.add(new Integer(i));
        }
        Collections.shuffle(list);
        for(int x = 0; x < 5; x++) {
        	Jokenpo_Controller th = new Jokenpo_Controller(list.get(ale.nextInt((5 - 1) + 1) + 1),list.get(ale.nextInt((10 - 6) + 1) + 6),x);
        	th.start();
        }
	}

}
