package Jokenpo;

import java.util.Random;

public class Jokenpo_Controller extends Thread {
	private int idTimeA,idTimeB,pontosA,pontosB,partida;
	private static int vitoriasA,vitoriasB,x;
	
	public Jokenpo_Controller(int idTimeA, int idTimeB,int partida) {
		this.idTimeA = idTimeA;
		this.idTimeB = idTimeB;
		this.partida = partida;
	}
	
	public void run() {
		jogo();
		if(x >= 5) {
			fimJogo();
		}
	}
	
	private void jogo(){
		int rodada = 0, resultado, jogadaA, jogadaB;
		Random ale = new Random();
		partida++;
		while(pontosA < 3 && pontosB < 3 && rodada < 5){
			System.out.println(partida + "ºPartida: " + "\nRodada:" + rodada + "\nTime A: Jogador #" + idTimeA + " VS " + " TimeB: Jogador #" + idTimeB + "\n");
			jogadaA = ale.nextInt(3);
			jogadaB = ale.nextInt(3);
			resultado = jogadaA + jogadaB;
			duelo(jogadaA,jogadaB,rodada,resultado);
			rodada++;
		}
		if(pontosA == 3) {
			
		}
		if(pontosB == 3) {
			
		}
		fimPartida();
	}
	private void fimPartida() {
		if(pontosA > pontosB) {
			vitoriasA++;
			System.out.println(partida + "ºPartida: " + "\nVitoria do time A!!!" + "\nTime A: " + vitoriasA + " VS " + " TimeB: " + vitoriasB + "\n");
			x++;
		}
		else {
			vitoriasB++;
			System.out.println(partida + "ºPartida: " + "\nVitoria do time B!!!" + "\nTime A: " + vitoriasA + " VS " + " TimeB: " + vitoriasB + "\n");
			x++;
		}
	}
	private void fimJogo() {
		if(vitoriasB > vitoriasA) {
			System.out.println("Fim de jogo!!!" + "\nVitoria do time B" + "\n");
		}
		else {
			System.out.println("Fim de jogo!!!" + "\nVitoria do time A" + "\n");
		}
	}
	private void duelo(int jogadaA, int jogadaB, int rodada, int resultado) {
		//Tesoura = 0
		//Papel = 1
		//Pedra = 2
		String op[]= {"Tesoura","Papel","Pedra"};
		if(resultado == 1 && jogadaA != jogadaB) {
			if(jogadaA < jogadaB ) {
				pontosA++;
				System.out.println(partida + "ºPartida: " + "\nRodada:" + rodada + "\nTime A: Jogador #" + idTimeA + " jogou: " + op[jogadaA] + " \nTime B: Jogador #" + idTimeB + " jogou: " + op[jogadaB]+ "\n");
				System.out.println(partida + "ºPartida: " + "\nRodada:" + rodada + "\nTime A: Jogador #" + idTimeA + " ganhou!!!" + "\nPlacar da rodada" +  "\nTime A: " + pontosA + " VS " + "TimeB: " + pontosB + "\n");
			}
			else {
				pontosB++;
				System.out.println(partida + "ºPartida: " + "\nRodada:" + rodada + "\nTime A: Jogador #" + idTimeA + " jogou: " + op[jogadaA] + " \nTime B: Jogador #" + idTimeB + " jogou: " + op[jogadaB] + "\n");
				System.out.println(partida + "ºPartida: " + "\nRodada:" + rodada + "\nTime B: Jogador #" + idTimeA + " ganhou!!!" + "\nPlacar da rodada" +  "\nTime A: " + pontosA + " VS " + "TimeB: " + pontosB + "\n");
			}
		}
		else if(resultado == 2 && jogadaA != jogadaB) {
			if(jogadaA > jogadaB) {
				pontosA++;
				System.out.println(partida + "ºPartida: " + "\nRodada:" + rodada + "\nTime A: Jogador #" + idTimeA + " jogou: " + op[jogadaA] + " \nTime B: Jogador #" + idTimeB + " jogou: " + op[jogadaB] + "\n");
				System.out.println(partida + "ºPartida: " + "\nRodada:" + rodada + "\nTime A: Jogador #" + idTimeA + " ganhou!!!" + "\nPlacar da rodada" +  "\nTime A: " + pontosA + " VS " + "TimeB: " + pontosB + "\n");
			}
			else {
				pontosB++;
				System.out.println(partida + "ºPartida: " + "\nRodada:" + rodada + "\nTime A: Jogador #" + idTimeA + " jogou: " + op[jogadaA] + " \nTime B: Jogador #" + idTimeB + " jogou: " + op[jogadaB] + "\n");
				System.out.println(partida + "ºPartida: " + "\nRodada:" + rodada + "\nTime B: Jogador #" + idTimeA + " ganhou!!!" + "\nPlacar da rodada" +  "\nTime A: " + pontosA + " VS " + " TimeB: " + pontosB + "\n");
			}
		}
		else if(resultado == 3 && jogadaA != jogadaB) {
			if(jogadaA < jogadaB) {
				pontosA++;
				System.out.println(partida + "ºPartida: " + "\nRodada:" + rodada + "\nTime A: Jogador #" + idTimeA + " jogou: " + op[jogadaA] + " \nTime B: Jogador #" + idTimeB + " jogou: " + op[jogadaB] + "\n");
				System.out.println(partida + "ºPartida: " + "\nRodada:" + rodada + "\nTime A: Jogador #" + idTimeA + " ganhou!!!" + "\nPlaca da rodadar" +  "\nTime A: " + pontosA + " VS " + " TimeB: " + pontosB + "\n");	
			}
			else {
				pontosB++;
				System.out.println(partida + "ºPartida: " + "\nRodada:" + rodada + "\nTime A: Jogador #" + idTimeA + " jogou: " + op[jogadaA] + " \nTime B: Jogador #" + idTimeB + " jogou: " + op[jogadaB] + "\n");
				System.out.println(partida + "ºPartida: " + "\nRodada:" + rodada + "\nTime B: Jogador #" + idTimeA + " ganhou!!!" + "\nPlacar da rodada" +  "\nTime A: " + pontosA + " VS " + " TimeB: " + pontosB + "\n");
			}
		}
		else if(jogadaA == jogadaB) {
			System.out.println(partida + "ºPartida: " + "\nRodada:" + rodada + "\nTime A: Jogador #" + idTimeA + " jogou: " + op[jogadaA] + " \nTime B: Jogador #" + idTimeB + " jogou: " + op[jogadaB] + "\n");
			System.out.println(partida + "ºPartida: " + "\nRodada:" + rodada + "\nempatou!!!" + "\nPlacar da rodada" +  "\nTime A: " + pontosA + " VS " + " TimeB: " + pontosB + "\n");
		}
	}
}
