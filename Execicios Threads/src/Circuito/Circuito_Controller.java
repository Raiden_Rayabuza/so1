package Circuito;

import java.util.Random;
import java.util.concurrent.Semaphore;

public class Circuito_Controller extends Thread {
	private int idAtleta;
	private static int ranking[][] = new int[25][2], pontos = 250, pontos2=250,x = 0;
	private Semaphore semaforo;
	public Circuito_Controller(int idAtleta, Semaphore semaforo){
		this.idAtleta = idAtleta;
		this.semaforo = semaforo;
	}
	
	public void run() {
		corrida();
		try {
			semaforo.acquire();
			tiroAlvo();
		} 
		catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			semaforo.release();
			ciclismo();
			if(x >= 25) {
				ranking();
			}
		}
	}
	
	void corrida(){
		Random ale = new Random();
		int distanciaPercorrida = 0;
		System.out.println("O Atleta #" + idAtleta + " come�ou a primeira parte da prova: Corrida");
		while(distanciaPercorrida < 3000) {
			distanciaPercorrida += ale.nextInt((25 - 20) + 1) + 20;
			try {
				Thread.sleep(30);
				System.out.println("O Atleta #" + idAtleta + " percorreu " + distanciaPercorrida + " metros");
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		ranking[idAtleta][0] = idAtleta;
		ranking[idAtleta][1] = pontos;
		pontos -= 10;
	}
	
	void tiroAlvo(){
		Random ale = new Random();
		int tentativas = 3, pontos = 0;
		System.out.println("O Atleta #" + idAtleta + " chegou na segunda parte da prova: Tiro ao alvo");
		while(tentativas > 0) {
			try {
				Thread.sleep(ale.nextInt((3000 - 500) + 1) + 500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			tentativas--;
		}
		pontos = ale.nextInt(11);
		ranking[idAtleta][1] += pontos;
		if(ranking[idAtleta][1] == 0) {
			System.out.println("O atleta #" + idAtleta + " errou o alvo e ganhou 0 pontos") ;
		}
		else {
			System.out.println("O atleta #" + idAtleta + " acertou o alvo e ganhou " + pontos + " pontos") ;
		}
	}
	
	void ciclismo(){
		Random ale = new Random();
		int distanciaPercorrida = 0;
		System.out.println("O Atleta #" + idAtleta + " chegou na terceira parte da prova: Ciclismo");
		while(distanciaPercorrida < 5000) {
			distanciaPercorrida += ale.nextInt((40 - 30) + 1) + 30;
			try {
				Thread.sleep(30);
				System.out.println("O Atleta #" + idAtleta + " pedalou " + distanciaPercorrida + " metros");
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		ranking[idAtleta][1] += pontos2;
		pontos2 -= 10;
		x++;
	}
	
	void ranking() {
		int  aux = 0;
		System.out.println("\nRanking\n");
		for(int x = 0; x < 25; x++) {
			for(int y = x + 1; y < 25; y++) {
				if(ranking[x][1] < ranking[y][1]) {
					aux = ranking[x][1];
					ranking[x][1] = ranking[y][1];
					ranking[y][1] = aux;
					
					aux = ranking[x][0];
					ranking[x][0] = ranking[y][0];
					ranking[y][0] = aux;
				}
			}
			System.out.println("Atleta #" + ranking[x][0] + " Pontos: " + ranking[x][1] + "\n");
		}
		System.out.println("O vencedor foi o atleta #" +ranking[0][0] + " Pontos: " + ranking[0][1]);
	}
}
