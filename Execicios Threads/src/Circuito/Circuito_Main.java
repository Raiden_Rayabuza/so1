package Circuito;

import java.util.concurrent.Semaphore;

class Circuito_Main {

	public static void main(String[] args) {
		Semaphore semaforo = new Semaphore(5);	
		for(int x = 0; x < 25; x++) {
			Circuito_Controller th = new Circuito_Controller(x,semaforo);
			th.start();
		}
	}
}
