package Aeroporto;

import java.util.concurrent.Semaphore;

public class Aeroporto_Main {

	public static void main(String[] args) {
		Semaphore semaforo = new Semaphore(2);	
		for(int x = 0; x < 12; x++) {
			Aeroporto_Controller th = new Aeroporto_Controller(x+1,semaforo);
			th.start();
		}

	}

}
