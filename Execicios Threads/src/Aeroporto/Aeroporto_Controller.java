package Aeroporto;

import java.util.Random;
import java.util.concurrent.Semaphore;

public class Aeroporto_Controller extends Thread {
	private int idAviao;
	private String pista;
	private static boolean norte,sul;
	Semaphore semaforo;
	
	public Aeroporto_Controller(int idAviao, Semaphore semaforo){
		this.idAviao = idAviao;
		this.semaforo = semaforo;
	}
	public void run(){
		try {
			manobrar();
			taxiar();
			semaforo.acquire();
			decolar();
		} 
		catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			afastarArea();
			semaforo.release();
		}
	}
	void manobrar(){
		Random ale = new Random();
		System.out.println("O avi�o #" + idAviao + " esta manobrando");
		try {
			Thread.sleep(ale.nextInt((7000 - 3000) + 1) + 3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	void taxiar(){
		Random ale = new Random();
		System.out.println("O avi�o #" + idAviao + " esta taxiando");
		try {
			Thread.sleep(ale.nextInt((10000 - 5000) + 1) + 5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	void decolar(){
		Random ale = new Random();
		try {
			Thread.sleep(ale.nextInt((4000 - 1000) + 1) + 1000);
			escolherPista();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	void afastarArea() {
		Random ale = new Random();
		System.out.println("O avi�o #" + idAviao + " esta se afastando do aeroporto");
		try {
			Thread.sleep(ale.nextInt((8000 - 3000) + 1) + 3000);
			desocuparPista();
		}catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	void escolherPista(){
		Random ale = new Random();
		int pista = ale.nextInt(2);
		if(pista == 0 && norte == false) {
			norte = true;
			this.pista = "Norte";
			System.out.println("O avi�o #" + idAviao + " vai decolar pela pista norte");
		}
		else if(pista == 0 && norte == true) {
			sul = true;
			this.pista = "Sul";
			System.out.println("O avi�o #" + idAviao + " tentou decolar pela pista norte, mas ela esta ocupada \nredirecionando o avi�o #" + idAviao + " para a pista sul");
		}
		else if(pista == 1 && sul == false) {
			sul = true;
			this.pista = "Sul";
			System.out.println("O avi�o #" + idAviao + " vai decolar pela pista sul");
		}
		else if(pista == 1 && sul == true) {
			norte = true;
			this.pista = "Norte";
			System.out.println("O avi�o #" + idAviao + " tentou decolar pela pista sul, mas ela esta ocupada \nredirecionando o avi�o #" + idAviao + " para a pista norte");
		}
	}
	
	void desocuparPista(){
		if(pista.equals("Norte")) {
			norte = false;
		}
		if(pista.equals("Sul")) {
			sul = false;
		}
		System.out.println("O avi�o #" + idAviao + " n�o esta mais presente no aeroporto");
	}
}
